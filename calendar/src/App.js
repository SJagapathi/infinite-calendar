import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import DateInput from './DateInput.js';
class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Calendar</h2>
        </div>
        <DateInput width="150px" />
      </div>
    );
  }
}

export default App;
