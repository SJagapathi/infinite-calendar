import React, { Component } from "react";
import Calendar from './Calendar.js';
import ShowSelectedDate from "./ShowSelectedDate.js";

export default class DateInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputGiven: false,
            givenDate: ""
        }
        this.handleDateSelection = this.handleDateSelection.bind(this);
    }

    handleDateSelection(event) {
        this.setState({ givenDate: event.target.value });
        if (this.state.givenDate !== "Invalid Date") {
            this.setState({ inputGiven: true });
        } else {
            this.setState({ inputGiven: false, givenDate: "" });
        }
    }

    handleDateSelect(date) {
        console.log(date.toLocaleString());
        let showDateDom = document.getElementById('showSelectedDate');
        showDateDom.innerHTML = "<p> You have selected <i> " + date.toDateString() + ".</i><p>"
    }

    render() {
        return (
            <form>
                <input type="date" placeholder="Enter start date" id="givenDate"
                    onChange={this.handleDateSelection} />
                {this.state.inputGiven === true &&
                    <Calendar startDate={this.state.givenDate}
                        width={this.props.width}
                        handleDateSelect={this.handleDateSelect} />}
                {this.state.inputGiven === true && <ShowSelectedDate />}
            </form>
        );
    }
} 