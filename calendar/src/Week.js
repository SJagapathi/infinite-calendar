import React, { Component } from 'react';
import Day from "./Day.js"
export default class Week extends Component {
    constructor(props) {
        super(props);
        this.getDate = this.getDate.bind(this);
    }
    getDate = (index) => {
        var today = new Date(this.props.startDate);
        today.setDate(today.getDate() + (this.props.weekNumber * 7 + index))
        return today;
    }
    render() {
        var days = [];
        for (var i = 0; i < 7; i++) {
            days.push(<Day key={this.getDate(i)} date={this.getDate(i)} />);
        }
        return (<div>{days}</div>);
    }
}